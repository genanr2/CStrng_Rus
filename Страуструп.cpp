﻿#include "pch.h"
#include <iostream>
#include "Страуструп.h"
УмнаяСтрока::УмнаяСтрока(const УмнаяСтрока& строка)
{
	строка.rep->n++;
	rep = строка.rep;
}
УмнаяСтрока::~УмнаяСтрока() 
{
	if (--rep->n == 0)
		delete rep;
}
УмнаяСтрока& УмнаяСтрока::operator=(const char* p) 
{
	if (rep->n == 1)
		rep->Assign(strlen(p), p);
	else
	{
		rep->n--;
		rep = new ДанныеСтроки(strlen(p), p);
	}
	return *this;
}
УмнаяСтрока& УмнаяСтрока::operator=(УмнаяСтрока& строка)
{
	строка.rep->n++;
	if (--rep->n == 0) delete rep;
	rep = строка.rep;
	return *this;
}
УмнаяСтрока::ДанныеСтроки::ДанныеСтроки(int nsz, const char* p)
{
	n = 1;
	sz = nsz;
	s = new char[sz + 1];
	strcpy_s(s, sz + 1,p);
}
УмнаяСтрока::ДанныеСтроки* УмнаяСтрока::ДанныеСтроки::GetOwnCopy()
{
	if (n == 1) return this;
	n--;
	return new ДанныеСтроки(sz, s);
}
void УмнаяСтрока::ДанныеСтроки::Assign(int nsz, const char* p)
{
	if (nsz != sz)
	{
		delete[] s;
		sz = nsz;
		s = new char[sz + 1];
	}
	strcpy_s(s, sz + 1, p);
}
std::ostream& operator<<(std::ostream&os, const УмнаяСтрока*строка)
{
	os << строка->rep->s;
	return os;
}
const char*УмнаяСтрока::ВозвратСтроки()
{
	return rep->s;
}
int УмнаяСтрока::ДлинаСтроки()
{
	return strlen(rep->s);
}
