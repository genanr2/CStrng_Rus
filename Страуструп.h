﻿#pragma once
#include <cstring>
#include <iostream>
#ifndef DEFINE_STRAUSTRING
#define DEFINE_STRAUSTRING

class УмнаяСтрока
{
	struct ДанныеСтроки
	{
		char* s;
		int n;
		int sz;
		ДанныеСтроки(int, const char*);
		~ДанныеСтроки() 
		{ 
			delete[] s;
			n = 0;
			sz = 0;
		}
		ДанныеСтроки* GetOwnCopy();
		void Assign(int, const char*);
	private:
		ДанныеСтроки(const ДанныеСтроки&);
//		ДанныеСтроки& operator=(const ДанныеСтроки&);
	};
	class Cref
	{
	public:
		inline Cref(УмнаяСтрока&, int);
		inline Cref(const Cref&);
//		Cref() {}
		inline operator char() const;
		inline void operator=(char);
	private:
		УмнаяСтрока& s;
		int i;
	};
public:
	inline УмнаяСтрока();
	inline УмнаяСтрока(const char*);
	УмнаяСтрока(const УмнаяСтрока&);
	~УмнаяСтрока();
	УмнаяСтрока& operator=(const char*);
	УмнаяСтрока& operator=(УмнаяСтрока&);
	inline Cref operator[](int);
	inline char operator[](int) const;

	const char*ВозвратСтроки();
	int  ДлинаСтроки();

	friend std::ostream&operator<<(std::ostream&, const УмнаяСтрока*);
private:
	inline void Write(int, char);
	inline int Size() const;
	inline char Read(int) const;
	inline void Check(int) const;
	class Range { };
	ДанныеСтроки* rep;
};
inline УмнаяСтрока::УмнаяСтрока() : rep(new ДанныеСтроки(0, "")) { }
inline УмнаяСтрока::УмнаяСтрока(const char* p) : rep(new ДанныеСтроки(strlen(p), p)) { }
inline void УмнаяСтрока::Check(int i) const
{
	if (i < 0 || rep->sz <= i) 
		throw Range();
}
inline char УмнаяСтрока::Read(int i) const
{
	return rep->s[i];
}
inline void УмнаяСтрока::Write(int i, char c)
{
	rep = rep->GetOwnCopy();
	rep->s[i] = c;
}
inline УмнаяСтрока::Cref УмнаяСтрока::operator[](int i)
{
	Check(i);
	return Cref(*this, i);
}
inline char УмнаяСтрока::operator[](int i) const
{
	Check(i);
	return rep->s[i];
}
inline int УмнаяСтрока::Size() const
{
	return rep->sz;
}
inline УмнаяСтрока::Cref::Cref(УмнаяСтрока& ss, int ii) :s(ss), i(ii)
{ 
}
inline УмнаяСтрока::Cref::Cref(const Cref& r) : s(r.s), i(r.i) {}
inline УмнаяСтрока::Cref::operator char() const
{
	s.Check(i);
	return s.Read(i);
}
inline void УмнаяСтрока::Cref::operator=(char c)
{
	s.Write(i, c);
}
#endif
